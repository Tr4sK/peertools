#!/usr/bin/python
'''
Source of inspiration :
https://framagit.org/Swedneck/peertube-autodiscover/-/blob/master/updateANode.py
When your instance in not accessible for some time, you will loose
federation.
Use this script to force resync of your federation.
'''
from urllib.request import Request, urlopen
from urllib.parse import urlencode
import json
import sys
import os


def get_following(node):
    '''
    Return a list of followed instance

        Parameters:
            node (str): Node to request data from
        Returns:
            node_following (str): list of followed instance
    '''
    url_api = "https://" + node + "/api/v1/server/following"
    node_count = json.loads(
        urlopen(
            Request(url_api + "?count=0"),
            timeout=15).read().decode())["total"]
    node_following = []
    for i in range(0, node_count, 100):
        for data in json.loads(
            urlopen(
                Request(url_api + "?count=100&start=" + str(i)),
                timeout=15).read().decode())["data"]:
            node_following.append(data["following"]["host"])
    return node_following


def get_followers(node):
    '''
    Return a list of followers of that instance
        Parameters:
            node (str): Node to request data from
        Returns:
            node_follower (str): List of follower
    '''
    url_api = "https://" + node + "/api/v1/server/followers"
    node_follower = []
    node_count = json.loads(urlopen(
        Request(url_api + "?count=0"),
        timeout=15).read().decode())["total"]
    for i in range(0, node_count, 100):
        for data in json.loads(urlopen(
                Request(url_api + "?count=100&start=" + str(i)),
                timeout=15).read().decode())["data"]:
            node_follower.append(data["follower"]["host"])
    return node_follower


os.chdir(os.path.dirname(os.path.realpath(__file__)))
if not os.path.exists("secret.json"):
    print("you don't have any key configuration, let's do one right know")
    node_name = input("node name (without https://) ? ")
    node_account = input("Account name (with admin right) ? ")
    node_password = input("Account password ? ")
    node_secret = {
        'node': node_name,
        'account': node_account,
        'password': node_password
    }
    # Serializing json.
    secret_json = json.dumps(node_secret, indent=4)
    with open('secret.json', mode='w', encoding='utf-8') as f:
        f.write(secret_json)
with open('secret.json', mode='r', encoding='utf-8') as f:
    key = json.loads(f.read())

refresh_list = []
ok_list = []
for remote_node in get_following(key["node"]):
    try:
        if key["node"] not in get_followers(remote_node):
            refresh_list.append(remote_node)
        else:
            ok_list.append(remote_node)
    except KeyboardInterrupt:
        raise Exception('Pass out this error.')
    except:
        sys.stderr.write("Error contacting " + remote_node + "\n")

print("Node that are following us : ")
print(ok_list)

print("Nodes that are NOT following us : ")
print(refresh_list)


r = json.loads(urlopen(
        Request("https://"+key["node"]+"/api/v1/oauth-clients/local"),
        timeout=15).read().decode())
token = json.loads(urlopen(
                Request("https://"+key["node"]+"/api/v1/users/token",
                        urlencode({
                            "client_id": r["client_id"],
                            "client_secret": r["client_secret"],
                            "grant_type": "password",
                            "response_type": "code",
                            "username": key["account"],
                            "password": key["password"]}).encode()),
                timeout=15).read().decode())
for node in refresh_list:
    print("Send DELETE for " + node)
    urlopen(
        Request("https://"+key["node"]+"/api/v1/server/following/" + node,
                headers={
                    "Authorization": "Bearer "+token["access_token"],
                    "Content-Type": "application/json"},
                method='DELETE'),
        timeout=15).read().decode()
urlopen(
        Request("https://"+key["node"]+"/api/v1/server/following",
                (json.dumps(
                    {"hosts": refresh_list})+"\n").encode("ascii"),
                headers={
                    "Authorization": "Bearer "+token["access_token"],
                    "Content-Type": "application/json"}),
        timeout=15).read().decode()
